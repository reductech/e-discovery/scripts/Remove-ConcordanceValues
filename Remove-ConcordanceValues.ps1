﻿<###############################################################################

MIT License

Copyright (c) 2018 Reductech Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

###############################################################################>

$InputPath  = 'c:\temp'
$OutputPath = 'c:\temp\out'

$ColumnOne = "BatesBegin"
$ColumnTwo = "ParentBates"

$FieldDelimiter  = [char]20  # DC4
$StringDelimiter = [char]254 # þ

# If column one and two have the same value, column two will be replace with:
$ReplaceWith = "${StringDelimiter}${StringDelimiter}"

################################################################################

$cOne = $StringDelimiter + $ColumnOne + $StringDelimiter
$cTwo = $StringDelimiter + $ColumnTwo + $StringDelimiter

################################################################################

if (!(Test-Path $OutputPath)) {
    throw "Can't find the output path: $OutputPath"
}

$files = Get-ChildItem -Path $InputPath -Filter *.dat

foreach ($file in $files) {

    try {

        $inStream = [System.IO.StreamReader]::new($file.FullName)

        $headerRow   = $inStream.ReadLine()
        $headerSplit = $headerRow -split $FieldDelimiter

        $headerOne   = -1
        $headerTwo   = -1

        for ($i = 0; $i -lt $headerSplit.Count; $i++) {
            if ($headerSplit[$i] -eq $cOne) {
                $headerOne = $i
            } elseif ($headerSplit[$i] -eq $cTwo) {
                $headerTwo = $i
            }
            if ($headerOne -ne -1 -and $headerTwo -ne -1) {
                break
            }
        }

        if ($headerOne -eq -1 -or $headerTwo -eq -1) {
            Write-Error "Can't find column One or column Two in the header of $($file.FullName)"
            $inStream.Close()
            continue
        }

        $outFile = Join-Path $OutputPath $file.Name

        $outStream = [System.IO.StreamWriter]::new($outFile)

        $outStream.WriteLine($headerRow)

        while (($line = $inStream.ReadLine()) -ne $null) {
            
            $split = $line -split $FieldDelimiter

            if ($split[$headerOne] -eq $split[$headerTwo]) {
                $split[$headerTwo] = $ReplaceWith
            }

            $outLine = $split -join $FieldDelimiter
            $outStream.WriteLine($outLine)

        }

    } catch {
        Write-Error "Could not process $($file.FullName)"
    } finally {
        if ($inStream -ne $null) {
            $inStream.Close()
        }
        if ($outStream -ne $null) {
            $outStream.Close()
        }
    }

}

################################################################################
